﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Management.Common.DTO
{
    public class TasksListForUserDTO
    {
        public UserDTO Performer { get; set; }
        public List<TaskDTO> Tasks { get; set; }
    }
}
