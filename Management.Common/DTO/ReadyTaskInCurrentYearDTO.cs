﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Management.Common.DTO
{
    public class ReadyTaskInCurrentYearDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
