﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Management.DAL.Interfaces;
using Management.DAL.Models;
using Microsoft.EntityFrameworkCore;

namespace Management.DAL.Repositories
{
    public class ProjectRepository : IRepository<Project>
    {
        private readonly ManagementDbContext _dataContext;
        public ProjectRepository(ManagementDbContext context)
        {
            _dataContext = context;
        }

        public IEnumerable<Project> GetAll()
        {
            return _dataContext.Projects;
        }

        public Project Get(int id)
        {
            return _dataContext.Projects.Find(id);
        }

        public void Create(Project entity)
        {
            _dataContext.Projects.Add(entity);
        }

        public void Update(Project entity)
        {
            _dataContext.Entry(entity).State = EntityState.Modified;
        }

        public IEnumerable<Project> Find(Func<Project, Boolean> predicate)
        {
            return _dataContext.Projects.Where(predicate).ToList();
        }

        public void Delete(Project entity)
        {
            Project project = _dataContext.Projects.Find(entity.Id);
            if (project != null)
                _dataContext.Projects.Remove(project);
        }
    }
}
