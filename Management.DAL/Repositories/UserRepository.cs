﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Management.DAL.Interfaces;
using Management.DAL.Models;
using Microsoft.EntityFrameworkCore;

namespace Management.DAL.Repositories
{
    public class UserRepository : IRepository<User>
    {
        private readonly ManagementDbContext _dataContext;
        public UserRepository(ManagementDbContext data)
        {
            _dataContext = data;
        }

        public IEnumerable<User> GetAll()
        {
            return _dataContext.Users;
        }

        public User Get(int id)
        {
            return _dataContext.Users.Find(id);
        }

        public void Create(User entity)
        {
            _dataContext.Users.Add(entity);
        }

        public void Update(User entity)
        {
            _dataContext.Entry(entity).State = EntityState.Modified;

        }

        public IEnumerable<User> Find(Func<User, Boolean> predicate)
        {
           return _dataContext.Users.Where(predicate).ToList();
        }

        public void Delete(User entity)
        {
            User user = _dataContext.Users.Find(entity.Id);
            if (user != null)
                _dataContext.Users.Remove(user);
        }
    }
}
