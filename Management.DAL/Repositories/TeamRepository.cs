﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Management.DAL.Interfaces;
using Management.DAL.Models;
using Microsoft.EntityFrameworkCore;

namespace Management.DAL.Repositories
{
    public class TeamRepository : IRepository<Team>
    {
        private readonly ManagementDbContext _dataContext;
        public TeamRepository(ManagementDbContext data)
        {
            _dataContext = data;
        }

        public IEnumerable<Team> GetAll()
        {
            return _dataContext.Teams; 
        }

        public Team Get(int id)
        {
            return _dataContext.Teams.Find(id);
        }

        public void Create(Team entity)
        {
            _dataContext.Teams.Add(entity);
        }

        public void Update(Team entity)
        {
            _dataContext.Entry(entity).State = EntityState.Modified;
        }

        public IEnumerable<Team> Find(Func<Team, Boolean> predicate)
        {
             return _dataContext.Teams.Where(predicate).ToList();
        }

        public void Delete(Team entity)
        {
            Team team = _dataContext.Teams.Find(entity.Id);
            if (team != null)
                _dataContext.Teams.Remove(team);
        }
    }
}
