﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Management.DAL.Interfaces;
using Management.DAL.Models;
using Microsoft.EntityFrameworkCore;

namespace Management.DAL.Repositories
{
    public class TaskRepository : IRepository<Task>
    {
        private readonly ManagementDbContext _dataContext;
        public TaskRepository(ManagementDbContext context)
        {
            _dataContext = context;
        }

        public IEnumerable<Task> GetAll()
        {
            return _dataContext.Tasks;
        }

        public Task Get(int id)
        {
            return _dataContext.Tasks.Find(id);
        }

        public void Create(Task entity)
        {
            _dataContext.Tasks.Add(entity);
        }

        public void Update(Task entity)
        {
            _dataContext.Entry(entity).State = EntityState.Modified;
        }
        public IEnumerable<Task> Find(Func<Task, Boolean> predicate)
        {
            return _dataContext.Tasks.Where(predicate).ToList();
        }

        public void Delete(Task entity)
        {
            Task task = _dataContext.Tasks.Find(entity.Id);
            if (task != null)
                _dataContext.Tasks.Remove(task);
        }
    }
}
