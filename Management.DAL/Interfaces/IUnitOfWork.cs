﻿using System;
using Management.DAL.Models;
using System.Collections.Generic;
using System.Text;

namespace Management.DAL.Interfaces
{
    public interface IUnitOfWork : IDisposable
    {
        IRepository<Project> ProjectsRepo { get; }
        IRepository<Task> TasksRepo { get; }
        IRepository<Team> TeamsRepo { get; }
        IRepository<User> UsersRepo { get; }

        void Save();
      
    }
}
