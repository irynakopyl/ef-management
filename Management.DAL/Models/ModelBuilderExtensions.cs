﻿using System;
using System.Collections.Generic;
using System.Text;
using Newtonsoft.Json;
using System.IO;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Management.DAL.Models
{
    public static class ModelBuilderExtensions
    {
        public static void Seed(this ModelBuilder modelBuilder)
        {
            var tasks = JsonConvert.DeserializeObject<List<DAL.Models.Task>>(File.ReadAllText(Path.Combine(Directory.GetParent(Directory.GetCurrentDirectory()).FullName, "Tasks.txt")));
            var users = JsonConvert.DeserializeObject<List<User>>(File.ReadAllText(Path.Combine(Directory.GetParent(Directory.GetCurrentDirectory()).FullName, "Users.txt")));
            var teams = JsonConvert.DeserializeObject<List<Team>>(File.ReadAllText(Path.Combine(Directory.GetParent(Directory.GetCurrentDirectory()).FullName, "Teams.txt")));
            var projects = JsonConvert.DeserializeObject<List<Project>>(File.ReadAllText(Path.Combine(Directory.GetParent(Directory.GetCurrentDirectory()).FullName, "Projects.txt")));
            
            modelBuilder.Entity<Team>().HasData(teams);
            modelBuilder.Entity<Task>().HasData(tasks);
            modelBuilder.Entity<User>().HasData(users);
            modelBuilder.Entity<Project>().HasData(projects);

        }
    }
}
