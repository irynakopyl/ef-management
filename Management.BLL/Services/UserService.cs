﻿using System;
using System.Collections.Generic;
using System.Text;
using Management.DAL.Interfaces;
using AutoMapper;
using Management.BLL.Services.Abstract;
using Management.Common.DTO;
using Management.DAL.Models;
using System.Linq;

namespace Management.BLL.Services
{
    public class UserService : BaseService
    {
        public UserService(IUnitOfWork context, IMapper mapper) : base(context, mapper)
        {

        }
        public UserDTO CreateUser(UserDTO newUser)
        {
            var userEntity = _mapper.Map<User>(newUser);
            _context.UsersRepo.Create(userEntity);
            _context.Save();
            var createdTask = _context.UsersRepo.Get(userEntity.Id);
            return _mapper.Map<UserDTO>(createdTask);
        }
        public ICollection<UserDTO> GetUsers()
        {
            var teams = _context.UsersRepo.GetAll().ToList();
            return _mapper.Map<ICollection<UserDTO>>(teams);
        }
        public UserDTO GetUserById(int id)
        {
            User user = _context.UsersRepo.Get(id);
            return _mapper.Map<UserDTO>(user);
        }
        public UserDTO UpdateUser(UserDTO user)
        {
            var result = _mapper.Map<User>(user);
            _context.UsersRepo.Update(result);
            _context.Save();
            return _mapper.Map<UserDTO>(_context.UsersRepo.Get(result.Id));
        }
        public void DeleteUser(int id)
        {
            var usr = _context.UsersRepo.Get(id);
            if (usr == null) throw new ArgumentException("Invalid id");
            _context.UsersRepo.Delete(usr);
            _context.Save();
        }
        public IDictionary<string, int> GetTaskAmountInProjects(int userId)
        {
            return _mapper.Map<ICollection<ProjectDTO>>(_context.ProjectsRepo.GetAll())
                .Select(
                p => (p, p.Tasks.Where(
                    x => x.PerformerId == userId).ToList().Count)).ToDictionary(p => p.p.Name, p => p.Item2);
        }
        public ICollection<ReadyTaskInCurrentYearDTO> GetAllTasksFinishedInCurrentYear(int userId)
        {
            return _mapper.Map<ICollection<ProjectDTO>>(_context.ProjectsRepo.GetAll()).SelectMany(
                p => p.Tasks.Where(x => x.TaskState == TaskStateDTO.Finished && x.FinishedAt.Year == 2020 && x.PerformerId == userId))
                .Select(t =>
               new ReadyTaskInCurrentYearDTO
               {
                   Id = t.Id,
                   Name = t.Name
               }).ToList();
        }
        public ICollection<TasksListForUserDTO> GetAllSortedUsersWithSortedTasks()
        {
            IEnumerable<TaskDTO> tasks = _mapper.Map<IEnumerable<TaskDTO>>(_context.TasksRepo.GetAll());
            IEnumerable<UserDTO> uzzers = _mapper.Map<IEnumerable<UserDTO>>(_context.UsersRepo.GetAll());
            return uzzers.GroupJoin(tasks,
                 user => user.Id,
                 task => task.PerformerId,
                (t, u) =>  new TasksListForUserDTO
                 {
                     Performer = t,
                     Tasks = u.OrderByDescending(t => t.Name.Length).ToList()
                 }
                ).OrderBy(t => t.Performer.FirstName).ToList();
        }
        public UserInfoDTO GetTasksAndProjectsInfo(int userId)
        {
            UserInfoDTO user = new UserInfoDTO() { };
            user.LastProject = _mapper.Map<ICollection<ProjectDTO>>(_context.ProjectsRepo.GetAll()).Where(p => p.AuthorId == userId).OrderByDescending(p => p.CreatedAt).First();//* Останній проект користувача(за датою створення)
            user.TaskAmountUnderLastProject = user.LastProject.Tasks.Count;//* Загальна кількість тасків під останнім проектом
            user.User = user.LastProject.Author;
            user.LongestTask = _mapper.Map<ICollection<ProjectDTO>>(_context.ProjectsRepo.GetAll()).SelectMany(p => p.Tasks.Where(p => p.PerformerId == userId)) //* Найтриваліший таск користувача за датою(найраніше створений - найпізніше закінчений)
               .Distinct()
               .OrderByDescending(t => t.FinishedAt - t.CreatedAt).First();

            user.NotReadyTasks = _mapper.Map<ICollection<ProjectDTO>>(_context.ProjectsRepo.GetAll()).SelectMany(p => p.Tasks.Where(
                p => (p.PerformerId == userId && (p.TaskState == TaskStateDTO.Canceled || p.TaskState == TaskStateDTO.Started)))).Distinct().Count();//* Загальна кількість незавершених або скасованих тасків для користувача
            return user;

        }
    }
}
